import {Recipe} from './recipe.model';
import {EventEmitter, Injectable} from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {

  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [
    new Recipe('Ratatouille', 'With a lot of cheese', 'http://acleanbake.com/wp-content/uploads/2015/08/IMG_9203.jpg',
    [
      new Ingredient('Dovlecel', 1),
      new Ingredient('Tomato', 5)
    ]),
    new Recipe('Carbonara', 'pasta', 'http://www.centercutcook.com/wp-content/uploads/2015/10/bucatini-carbonara-3.jpg',
      [
        new Ingredient('Pasta', 250),
        new Ingredient('Speck', 7)
      ]
    )];

  constructor(private slService: ShoppingListService){}

  getRecipes() {
    return this.recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

}
